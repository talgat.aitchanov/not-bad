package ee.notbad.exception;

public class InitialisationException extends RuntimeException {

    public InitialisationException(String message) {
        super(message);
    }

    public static void throwInitialisationException(Class<?> clazz) {
        throw new InitialisationException(clazz.getCanonicalName() + " cannot be initialised");
    }
}
