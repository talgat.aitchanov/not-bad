package ee.notbad.exception;

public final class AppException extends Exception {

    public AppException(String message) {
        super(message);
    }

    public AppException(String message, Throwable throwable) {
        super(message, throwable);
    }


    /**
     * @should throw AppException
     */
    public static void throwAppException(String message) throws AppException {
        throw new AppException(message);
    }

    public static void throwAppException(String message, Throwable throwable) throws AppException {
        throw new AppException(message, throwable);
    }

}
