package ee.notbad.service;

import ee.notbad.exception.AppException;
import lombok.extern.log4j.Log4j2;

import java.time.OffsetDateTime;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

@Log4j2
public class EventProducer {

    private static final ScheduledExecutorService SCHEDULED_EXECUTOR_SERVICE = Executors.newSingleThreadScheduledExecutor();

    private EventProducer() {
    }

    public static EventProducer getInstance() {
        return SingletonHelper.INSTANCE;
    }

    public void start() {
        SCHEDULED_EXECUTOR_SERVICE.scheduleAtFixedRate(() -> {
            try {
                long currentTimestamp = OffsetDateTime.now().toEpochSecond();
                log.info(Thread.currentThread().getName() + ". Produced " + currentTimestamp);
                EventQueue.put(String.valueOf(currentTimestamp));
            } catch (AppException e) {
                log.error(e.getMessage(), e);
            }
        }, 0, 1, TimeUnit.SECONDS);
    }

    private static class SingletonHelper {
        private static final EventProducer INSTANCE = new EventProducer();
    }

}
