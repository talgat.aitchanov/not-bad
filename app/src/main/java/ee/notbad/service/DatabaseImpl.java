package ee.notbad.service;

import ee.notbad.configuration.HikariCPDataSource;
import lombok.extern.log4j.Log4j2;

import java.sql.*;

@Log4j2
public class DatabaseImpl implements DatabaseService<String> {

    public static final String EVENTS = "events";
    private static final String CREATE_EVENTS_TABLE = "create table if not exists " + EVENTS + "(timestamp varchar(16))";
    private static final String SELECT_ALL_FROM = "select * from " + EVENTS;
    private static final String INSERT_STATEMENT = "insert into " + EVENTS + "(timestamp) values(%s)";

    @Override
    public void init() {
        try (Connection connection = HikariCPDataSource.getConnection()) {
            createTable(connection);
        } catch (SQLException e) {
            log.error(e.getMessage(), e);
        }
    }

    @Override
    public void printRows() {
        try (Connection connection = HikariCPDataSource.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SELECT_ALL_FROM)) {
            ResultSet resultSet = preparedStatement.executeQuery();
            ResultSetMetaData resultSetMetaData = resultSet.getMetaData();
            int columnsNumber = resultSetMetaData.getColumnCount();
            String[] values = new String[columnsNumber];
            while (resultSet.next()) {
                for (int currentIndex = 1; currentIndex <= columnsNumber; currentIndex++) {
                    values[currentIndex - 1] = resultSet.getString(currentIndex);
                }
                log.info(String.join(", ", values));
            }
        } catch (SQLException e) {
            log.error(e.getMessage(), e);
        }
    }

    @Override
    public void insert(String row) {
        try (Connection connection = HikariCPDataSource.getConnection();
             Statement statement = connection.createStatement()) {
            String insertStatement = String.format(INSERT_STATEMENT, row);
            log.info("Executing: " + insertStatement);
            statement.execute(insertStatement);
        } catch (SQLException e) {
            log.error(e.getMessage(), e);
        }
    }

    private void createTable(Connection connection) throws SQLException {
        try (Statement statement = connection.createStatement()) {
            log.info("Executing: " + CREATE_EVENTS_TABLE);
            statement.execute(CREATE_EVENTS_TABLE);
        }
    }

}
