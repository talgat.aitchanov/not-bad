package ee.notbad.service;

import ee.notbad.exception.AppException;
import ee.notbad.exception.InitialisationException;
import lombok.extern.log4j.Log4j2;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

@Log4j2
public class EventQueue {

    private static final BlockingQueue<String> UNBOUNDED_QUEUE = new LinkedBlockingQueue<>();

    private EventQueue() {
        InitialisationException.throwInitialisationException(EventQueue.class);
    }

    public static void put(String timestamp) throws AppException {
        try {
            UNBOUNDED_QUEUE.put(timestamp);
        } catch (InterruptedException e) {
            log.error(e.getMessage(), e);
            Thread.currentThread().interrupt();
            throw new AppException(e.getMessage(), e);
        }
    }

    public static String take() throws AppException {
        try {
            return UNBOUNDED_QUEUE.take();
        } catch (InterruptedException e) {
            log.error(e.getMessage(), e);
            Thread.currentThread().interrupt();
            throw new AppException(e.getMessage(), e);
        }
    }

}
