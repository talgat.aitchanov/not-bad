package ee.notbad.service;

import ee.notbad.exception.AppException;
import lombok.extern.log4j.Log4j2;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

@Log4j2
public class EventConsumer {

    private static final ScheduledExecutorService SCHEDULED_EXECUTOR_SERVICE = Executors.newSingleThreadScheduledExecutor();

    private EventConsumer() {
    }

    public static EventConsumer getInstance() {
        return SingletonHelper.INSTANCE;
    }

    public void start(DatabaseService<String> ds) {
        SCHEDULED_EXECUTOR_SERVICE.scheduleAtFixedRate(() -> {
            try {
                String a = EventQueue.take();
                log.info(Thread.currentThread().getName() + ". Consumed " + a);
                ds.insert(a);
            } catch (AppException e) {
                log.error(e.getMessage(), e);
            }
        }, 0, 1, TimeUnit.SECONDS);
    }

    private static class SingletonHelper {
        private static final EventConsumer INSTANCE = new EventConsumer();
    }

}
