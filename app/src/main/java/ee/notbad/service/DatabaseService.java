package ee.notbad.service;

public interface DatabaseService<T> {

    void init();

    void printRows();

    void insert(T row);

}
