package ee.notbad;


import ee.notbad.service.DatabaseImpl;
import ee.notbad.service.DatabaseService;
import ee.notbad.service.EventConsumer;
import ee.notbad.service.EventProducer;
import lombok.extern.log4j.Log4j2;

import java.util.Arrays;

@Log4j2
public class App {

    private static final String P = "-p";

    public static void main(String[] args) {
        DatabaseService<String> databaseService = new DatabaseImpl();
        databaseService.init();

        if (Arrays.asList(args).contains(P)) {
            log.info("print");
            databaseService.printRows();
        } else {
            EventConsumer.getInstance().start(databaseService);
            EventProducer.getInstance().start();
        }
    }
}
