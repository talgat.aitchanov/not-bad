package ee.notbad;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class AppTest {

    @Test
    void main_withArgument_success() {
        Assertions.assertDoesNotThrow(() -> App.main(new String[]{"-p"}));
    }

    @Test
    void main_withoutArgument_success() {
        Assertions.assertDoesNotThrow(() -> App.main(new String[]{}));
    }

}
